import pubmed_parser as pp
from multiprocessing import Pool, Queue, cpu_count
from pymongo import MongoClient


class XmlToMongo:
    def __init__(self, path):
        self.count = cpu_count()
        self.config = {
            'host': 'localhost',
            'port': 27017,
            'db': 'articles',
            'alias': 'default',
            'username': 'mongouser',
            'password': 'super-password',
            'maxPoolSize': 100,
            'authMechanism': 'SCRAM-SHA-1',
            'connect': False,
        }
        self.collection_name = 'publication'
        self.queue = Queue()
        self.pool = Pool(self.count, self.worker_main)
        self.path_xmls = pp.list_xml_path(path)

    def write_to_queue(self):
        for path in self.path_xmls:
            self.queue.put(path)

    def worker_main(self):
        client = MongoClient(host=self.config.get('host'),
                             port=self.config.get('port'),
                             maxPoolSize=int(self.config.get('maxPoolSize', 50)),
                             # username=self.config.get('username'),
                             # password=self.config.get('password'),
                             # authMechanism=self.config.get('authMechanism', 'SCRAM-SHA-1'),
                             # authSource=self.config.get('db'),
                             connect=self.config.get('connect', False))
        db = client[self.config.get('db')]
        collection = db[self.collection_name]
        while True:
            xml_path = self.queue.get()
            pubmed_dict = pp.parse_pubmed_xml(xml_path)
            artcl = {'pmid': pubmed_dict['pmid'],
                    'title': pubmed_dict['full_title'],
                    'abstract': pubmed_dict['abstract'],
                    'publication_year': pubmed_dict['publication_year'],
                    'publication_date': pubmed_dict['publication_date'],
                    }
            collection.insert_one(artcl)
            if self.queue.qsize() == 0:
                break


