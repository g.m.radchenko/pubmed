import os
import tarfile
import shutil
from xml_to_mongo import XmlToMongo
import wget


class MakeDB:
    def __init__(self):
        self.cur_dir = os.path.join(os.getcwd(), 'data_pm')

    def _make_dir(self):
        if not os.path.exists(self.cur_dir):
           print("The path doesn't exist. Making it")
           os.makedirs(self.cur_dir)

    def main(self):
        self._make_dir()
        ftp = 'ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/oa_bulk/'
        archives = ['comm_use.A-B.xml.tar.gz', 'comm_use.C-H.xml.tar.gz', 'comm_use.I-N.xml.tar.gz',
                    'comm_use.O-Z.xml.tar.gz', 'non_comm_use.A-B.xml.tar.gz', 'non_comm_use.C-H.xml.tar.gz',
                    'non_comm_use.I-N.xml.tar.gz', 'non_comm_use.O-Z.xml.tar.gz']

        for file in archives:
            fname = os.path.join(self.cur_dir, file)
            print(file, " downloading...")
            wget.download(ftp + file, out=self.cur_dir)
            print(file, " downloaded")
            tar = tarfile.open(fname, "r:gz")
            print(file, " extracting...")
            try:
                tar.extractall(self.cur_dir)
            except EOFError:
                pass
            print(file, " extracted")
            tar.close()
            os.remove(fname)
            print(file, " parsing...")
            xm = XmlToMongo(self.cur_dir)
            xm.write_to_queue()
            xm.pool.close()
            xm.pool.join()
            print(file, " parsed and saved in Mongo")
            for folder in os.listdir(self.cur_dir):
                shutil.rmtree(os.path.join(self.cur_dir, folder))


if __name__ == '__main__':
    mdb = MakeDB()
    mdb.main()
