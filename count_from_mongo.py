import pymongo as pm
import csv


class MongoSearch:
    def __init__(self):
        self.config = {
            'host': 'localhost',
            'port': 27017,
            'db': 'articles',
            'alias': 'default',
            'username': 'mongouser',
            'password': 'super-password',
            'maxPoolSize': 100,
            'authMechanism': 'SCRAM-SHA-1',
            'connect': False,
        }
        self.collection_name = 'publication'

    def main(self):
        client = pm.MongoClient(host=self.config.get('host'),
                                 port=self.config.get('port'),
                                 maxPoolSize=int(self.config.get('maxPoolSize', 50)),
                                 # username=config.get('username'),
                                 # password=config.get('password'),
                                 # authMechanism=config.get('authMechanism', 'SCRAM-SHA-1'),
                                 # authSource=config.get('db'),
                                 connect=self.config.get('connect', False))
        db = client[self.config.get('db')]
        collection = db[self.collection_name]
        collection.create_index([("title", "text")])
        ctypes = [['Lung'], ['Gastric', 'Stomach'], ['Colon', 'Bowel', 'Colorectal'],
                  ['Liver', 'Hepatocellular', 'Hepatic'], ['Lobular', 'Breast'], ['Esophageal'], ['Pancreatic'],
                  ['Prostate'], ['Leukaemia', 'Leukemia'], ['Cervical'], ['Non-Hodgkin'], ['Brain'], ['Bladder'],
                  ['Lip', 'Oral'], ['Ovarian'], ['Kidney', 'Renal'], ['Pharyngeal'], ['Laryngeal'], ['Gallbladder']]

        with open('publications.csv', 'w+', newline='') as csvfile:
            fieldnames = ['cancer_type', 'number_publications', 'year']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for year in range(2005, 2019):
                for cancer_type in ctypes:
                    if cancer_type[0] not in ['Leukaemia', 'Non-Hodgkin']:
                        count = 0
                        exclude_count = 0
                        for name in cancer_type:
                            exclude_dict = {}
                            for i in range(exclude_count):
                                exclude_dict.update({i: cancer_type[i]})
                            record = collection.find(
                                {"$and": [{"publication_year": '{}'.format(str(year))},
                                          {"$text": {"$search": '"{0}" "cancer" -{1} -{2}'.format(
                                              name, exclude_dict.get(0, ""), exclude_dict.get(1, ""))}}]}).count()
                            count += record
                            record = collection.find(
                                {"$and": [{"publication_year": '{}'.format(str(year))},
                                          {"$text": {"$search": '"{0}" "carcinoma" -cancer -{1} -{2}'.format(
                                              name, exclude_dict.get(0, ""), exclude_dict.get(1, ""))}}]}).count()
                            count += record
                            record = collection.find(
                                {"$and": [{"publication_year": '{}'.format(str(year))},
                                          {"$text": {"$search": '"{0}" "tumor" -cancer -carcinoma -{1} -{2}'.format(
                                              name, exclude_dict.get(0, ""), exclude_dict.get(1, ""))}}]}).count()
                            count += record
                            exclude_count += 1
                        writer.writerow({'cancer_type': name, 'number_publications': count, 'year': str(year)})
                    else:
                        count = 0
                        exclude_count = 0
                        for name in cancer_type:
                            exclude_dict = {}
                            for i in range(exclude_count):
                                exclude_dict.update({i: cancer_type[i]})
                            record = collection.find(
                                {"$and": [{"publication_year": '{}'.format(str(year))},
                                          {"$text": {"$search": '"{0}" -{1} -{2}'.format(
                                              name, exclude_dict.get(0, ""), exclude_dict.get(1, ""))}}]}).count()
                            count += record
                            exclude_count += 1
                        writer.writerow({'cancer_type': name, 'number_publications': count, 'year': str(year)})
        client.close()


if __name__ == '__main__':
    mdb = MongoSearch()
    mdb.main()





